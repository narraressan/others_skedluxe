
var skedluxeApp = angular.module("skedluxeApp", ["ngRoute", "ui.bootstrap", "ngStorage", "angular-loading-bar", "ngAnimate", "blockUI"]);

skedluxeApp.config(function($routeProvider, cfpLoadingBarProvider, blockUIConfig){
	$routeProvider
	.when("/signup", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id != undefined &&
					String($localStorage.user_id).trim() != "" &&
					String($localStorage.user_id).trim() != "undefined"){
					$location.path("/" + $localStorage.access_url[0]);
				}
			}
		},
		templateUrl: "Back-End/pages/signup.html"
	})
	.when("/plans", { templateUrl: "Back-End/pages/plans.html"})
	.when("/login", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id != undefined &&
					String($localStorage.user_id).trim() != "" &&
					String($localStorage.user_id).trim() != "undefined"){
					$location.path("/" + $localStorage.access_url[0]);
				}
			}
		},
		templateUrl: "Back-End/pages/login.html"
	})
	.when("/free-dashboard", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id == undefined || 
					String($localStorage.user_id).trim() == "" || 
					String($localStorage.user_id).trim() == "undefined"){
					$location.path("/login");
				}

				// check user access in this url
				if($localStorage.access_url != undefined){ 
					if ($localStorage.access_url.indexOf("free-dashboard") == -1) {
						$location.path("/403");
					}
				}
			}
		},
		templateUrl: "Back-End/pages/free-dashboard.html"
	})
	.when("/student-dashboard", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id == undefined || 
					String($localStorage.user_id).trim() == "" || 
					String($localStorage.user_id).trim() == "undefined"){
					$location.path("/login");
				}

				// check user access in this url
				if($localStorage.access_url != undefined){ 
					if ($localStorage.access_url.indexOf("student-dashboard") == -1) {
						$location.path("/403");
					}
				}
			}
		},
		templateUrl: "Back-End/pages/student-dashboard.html"
	})
	.when("/solo-instructor-dashboard", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id == undefined || 
					String($localStorage.user_id).trim() == "" || 
					String($localStorage.user_id).trim() == "undefined"){
					$location.path("/login");
				}

				// check user access in this url
				if($localStorage.access_url != undefined){ 
					if ($localStorage.access_url.indexOf("solo-instructor-dashboard") == -1) {
						$location.path("/403");
					}
				}
			}
		},
		templateUrl: "Back-End/pages/solo-instructor-dashboard.html"
	})
	.when("/business-plan-dashboard", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id == undefined || 
					String($localStorage.user_id).trim() == "" || 
					String($localStorage.user_id).trim() == "undefined"){
					$location.path("/login");
				}

				// check user access in this url
				if($localStorage.access_url != undefined){ 
					if ($localStorage.access_url.indexOf("business-plan-dashboard") == -1) {
						$location.path("/403");
					}
				}
			}
		},
		templateUrl: "Back-End/pages/business-plan-dashboard.html"
	})
	.when("/enterprise-plan-dashboard", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id == undefined || 
					String($localStorage.user_id).trim() == "" || 
					String($localStorage.user_id).trim() == "undefined"){
					$location.path("/login");
				}

				// check user access in this url
				if($localStorage.access_url != undefined){ 
					if ($localStorage.access_url.indexOf("enterprise-plan-dashboard") == -1) {
						$location.path("/403");
					}
				}
			}
		},
		templateUrl: "Back-End/pages/enterprise-plan-dashboard.html"
	})
	.when("/resend/lost/credentials", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id != undefined &&
					String($localStorage.user_id).trim() != "" &&
					String($localStorage.user_id).trim() != "undefined"){
					$location.path("/" + $localStorage.access_url[0]);
				}
			}
		},
		templateUrl: "Back-End/pages/forgot-pass.html"
	})
	.when("/:business_name/login", { 
		resolve: {
			"check": function($location, $localStorage){
				if($localStorage.user_id != undefined &&
					String($localStorage.user_id).trim() != "" &&
					String($localStorage.user_id).trim() != "undefined"){
					$location.path("/404");
				}
			}
		},
		templateUrl: "Back-End/pages/loginBusiness.html"
	})
	.when("/403", { templateUrl: "Back-End/pages/403.html"})
	.when("/404", { templateUrl: "Back-End/pages/404.html"})
	.when("/500", { templateUrl: "Back-End/pages/500.html"})
	.when("/503", { templateUrl: "Back-End/pages/503.html"})
	.when("/504", { templateUrl: "Back-End/pages/504.html"})
	.otherwise({ redirectTo: "/404" });

	cfpLoadingBarProvider.includeSpinner = false;
	blockUIConfig.message = "";
});

// REMEMBER THAT DURING RUN -- EVERYTHING IN THIS FUNCTION IS CALLED
skedluxeApp.run(function($http, $location, $localStorage, $rootScope){
	$rootScope.logout_func_trigger = function(){
		var bridgeRemember = $localStorage.rememberMe;

		if(bridgeRemember == true){
			var userN = $localStorage.username;
			var passW = $localStorage.password;

			$localStorage.$reset();

			$localStorage.rememberMe = bridgeRemember;
			$localStorage.username = userN;
			$localStorage.password = passW;
		}
		else{ $localStorage.$reset();}

		$location.path("/login");
	};
});

// plans controller
skedluxeApp.controller("skedluxeApp_plans_ctrl", function($scope, $http, $location, $localStorage){
	// initialize local variables

	// $scope functions
	$scope.plans_func_generateCode = function(id){
		var permitCode = (new Date().getTime()).toString(12);
		// console.log(id);

		var lists = {"id": id, "code": permitCode};

		var postData = "skedluxeData="+JSON.stringify(lists);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/new-permit-code",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				if(String(response.data).trim() != "200"){
					$.snackbar({content: "Oops, this ticket's code has been sold just now. Please generate a new code.", timeout: 5000});
				}
				else{ 
					$.snackbar({content: "Copy this code " + permitCode, timeout: 10000});
					$scope.plans_var_generatedCode = permitCode;
				}
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, this ticket's code has been sold just now. Please generate a new code.", timeout: 5000});
				$location.path("/404");
			}
		);
	}
	$scope.toSignup = function(){
		$("#generateCodec").modal("hide");
		setTimeout(function(){ document.location.href = "#/signup"}, 500);
	}
	function plans_func_getPlans(){
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/plans",
			data: "",
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){ 
				var temp = response.data;
				// console.log(temp);

				$scope.nonPremium = [];
				$scope.oneTimePay = [];
				$scope.lowmonthly = [];
				$scope.popular = [];
				$scope.highmonthly = [];

				for (var i = 0; i < temp.length; i++) {
					if(!isNaN(temp[i]["billed_annually"])){
						var cost = parseInt(temp[i]["billed_annually"]);
						temp[i].inclusions = JSON.parse(temp[i].inclusions);

						if(temp[i]["name"] == "Free User"){ $scope.nonPremium.push(temp[i]);}
						else if(temp[i]["name"] == "Student"){ $scope.oneTimePay.push(temp[i]);}
						else if(cost >= 10 && cost <= 100) { $scope.lowmonthly.push(temp[i]);}
						else if(cost >= 100 && cost <= 300){ $scope.popular.push(temp[i]);}
						else{ $scope.highmonthly.push(temp[i]);}
					}
					else{ 
						temp[i].inclusions = JSON.parse(temp[i].inclusions);
						$scope.highmonthly.push(temp[i]); 
					}
				}
			},
			function errorCallback(response){
				$.snackbar({content: "Unable to load service packages.", timeout: 5000});
				$location.path("/404");
			}
		);
	}

	// life cycle
	$.material.init();
	plans_func_getPlans();
});

// signup controller
skedluxeApp.controller("skedluxeApp_signup_ctrl", function($scope, $http, $location, $localStorage){
	// initialize local variables
	$scope.signup_var_permitcode_notfree = false;
	$scope.signup_var_dontAgree2Terms = true;
	var lists = { username: "", password: "", fullname: "", contact: "", email: "", license: "", permitcode: ""};
	
	// $scope functions
	$scope.signup_func_getRoleSelected = function(){
		if($scope.signup_select_roleOptions != "Free User"){ $scope.signup_var_permitcode_notfree = true;}
		else{ $scope.signup_var_permitcode_notfree = false;}
	}
	$scope.signup_func_agree2Terms = function(){
		if($scope.signup_var_dontAgree2Terms == true){ $scope.signup_var_dontAgree2Terms = false;}
		else{ $scope.signup_var_dontAgree2Terms = true;}
	}
	$scope.signup_func_submitForm = function(){
		lists.username = String($scope.signup_input_usern).trim();
		lists.password = String($scope.signup_input_pass).trim();
		lists.fullname = String($scope.signup_input_fulln).trim();
		lists.contact = String($scope.signup_input_mobileno).trim();
		lists.email = String($scope.signup_input_email).trim();
		lists.license = String($scope.signup_input_license).trim();
		lists.permitcode = String($scope.signup_input_permitcode).trim();

		var noError = false;

		if(String($scope.signup_input_usern).trim() != "undefined" && String($scope.signup_input_pass).trim() != "undefined" && String($scope.signup_input_fulln).trim() != "undefined" && String($scope.signup_input_mobileno).trim() != "undefined" && String($scope.signup_input_email).trim() != "undefined" && String($scope.signup_select_roleOptions).trim() != "undefined"){
			
			if(String($scope.signup_select_roleOptions).trim() != "Free User"){
				if(String($scope.signup_input_permitcode).trim() != "undefined" && $scope.signup_input_permitcode != "undefined" && String($scope.signup_input_permitcode).trim() != ""){ 
					noError = true;
				}
				else{
					noError = false;
					$.snackbar({content: "Please complete the form. Thank you.", timeout: 5000});
				}
			}

			if(String($scope.signup_select_roleOptions).trim() == "Free User"){
				if(String($scope.signup_input_permitcode).trim() == "undefined" || $scope.signup_input_permitcode == "undefined" || String($scope.signup_input_permitcode).trim() == ""){ 
					noError = true;
				}
				else{
					noError = false;
					$.snackbar({content: "I am detecting a permit code. Please remove it.", timeout: 5000});
				}
			}

			if(noError){
				var postData = "skedluxeData="+JSON.stringify(lists);
				$http({
					method : "POST",
					url : "http://localhost/narra/skedluxe/api/signup",
					data: postData,
					headers : {"Content-Type": "application/x-www-form-urlencoded"}
				})
				.then(
					function successCallback(response){
						// console.log(response.data)
						if(String(response.data).trim() == "200"){
							$.snackbar({content: "Success! You will now be directed to login.", timeout: 5000});
							$.snackbar({content: "We sent you an email. Check it for updates, promos and more.", timeout: 5000});
							$location.path("/login");
						}
						else{ 
							$.snackbar({content: "Oops, It seems someone I know is using either of your username, permit code or email.", timeout: 5000});
						}
					},
					function errorCallback(response){ 
						$.snackbar({content: "I am detecting a strange login process. I am treating this as harmful.", timeout: 5000});
						$location.path("/"+String(response.status).trim());
					}
				);
			}
		}
		else{ $.snackbar({content: "You provided an invalid credential format. Please review your form.", timeout: 5000});}
	}
	function signup_func_getRoles(){
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/roles",
			data: "",
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){ 
				var temp = String(response.data).trim().split(",");
				$scope.signup_var_roles = temp;
				// console.log(temp)
			},
			function errorCallback(response){
				$.snackbar({content: "Unable to load user roles.", timeout: 5000});
				// $location.path("/"+String(response.status).trim());
			}
		);
	}

	// life cycle
	$.material.init();
	$("select").dropdown();
	signup_func_getRoles();
});

// forgot password
skedluxeApp.controller("skedluxeApp_forgotpassword_ctrl", function($scope, $http, $location, $localStorage){
	// initialize local variables

	// $scope functions
	$scope.remPass_func_submitForm = function(){
		if($scope.remPass_var_email != undefined){
			var lists = {"email": $scope.remPass_var_email}
			var postData = "skedluxeData="+JSON.stringify(lists);
				$http({
					method : "POST",
					url : "http://localhost/narra/skedluxe/api/list/recover/credential",
					data: postData,
					headers : {"Content-Type": "application/x-www-form-urlencoded"}
				})
				.then(
					function successCallback(response){ 
						// console.log(String(response.data).trim());
						if(String(response.data).trim() == "200"){
							$.snackbar({content: "We sent you an email with your username and password.", timeout: 5000});
							$location.path("/login");
						}
						else{ 
							$.snackbar({content: "Sorry, I don't recognize that email.", timeout: 5000});
						}
					},
					function errorCallback(response){ 
						$.snackbar({content: "I am detecting a strange login process. I am treating this as harmful.", timeout: 5000});
						$location.path("/"+String(response.status).trim());
					}
				);
		}
	}

	// life cycle
	$.material.init();
});

// login controller
skedluxeApp.controller("skedluxeApp_login_ctrl", function($scope, $http, $location, $localStorage){
	// initialize local variables
	var loginCredentials = {username: "", password: ""};

	$scope.rememberMe = $localStorage.rememberMe;
	$scope.login_input_usern = $localStorage.username;
	$scope.login_input_pass = $localStorage.password;

	// $scope functions
	$scope.login_func_submitForm = function(){
		loginCredentials.username = $scope.login_input_usern;
		loginCredentials.password = $scope.login_input_pass;

		if(String(loginCredentials.username).trim() != "undefined" && String(loginCredentials.password).trim() != "undefined"){
			var postData = "skedluxeData="+JSON.stringify(loginCredentials);
			$http({
				method : "POST",
				url : "http://localhost/narra/skedluxe/api/list/user",
				data: postData,
				headers : {"Content-Type": "application/x-www-form-urlencoded"}
			})
			.then(
				function successCallback(response){ 
					// var temp = String(response.data).trim().split(",");
					var temp = response.data
					
					if(parseInt(temp[0]) == 1){
						if (temp[1] != "" && temp[1] != "undefined" && temp[1] != undefined) {
							// start user session
							$localStorage.user_id = temp[1];
							$localStorage.username = temp[2];
							$localStorage.password = temp[3];
							$localStorage.fullname = temp[4];
							$localStorage.user_contact_no = temp[5];
							$localStorage.user_email = temp[6];
							$localStorage.user_license_no = temp[7];
							$localStorage.user_detail_json = temp[8];
							$localStorage.permit_code = temp[9];
							$localStorage.plan_name = temp[10];
							$localStorage.access_url = (String(temp[11]).trim()).split("/");
							$location.path("/" + $localStorage.access_url[0]);
						}
						else{ $.snackbar({content: "This account doesn't exist, check your credentials or try signing up.", timeout: 5000});}
					}
					else{ 
						$location.path("/403");
						$localStorage.$reset();
					}
				},
				function errorCallback(response){ $location.path("/"+String(response.status).trim());}
			);
		}
		else{ $.snackbar({content: "You didn't enter a correct username & password format.", timeout: 5000});}
	}
	$scope.login_func_rememberMe = function(){
		if($localStorage.rememberMe == true){ $localStorage.rememberMe = false;}
		else{ $localStorage.rememberMe = true}
		
		$scope.rememberMe = $localStorage.rememberMe;
	}

	// life cycle
	$.material.options.autofill = true;
	$.material.init();
});

// Employee login controller
skedluxeApp.controller("skedluxeApp_employee_login_ctrl", function($scope, $http, $location, $localStorage){
	// initialize local variables
	var loginCredentials = {email: "", password: "", company: ""};

	var thisURL = String(window.location.href).trim().split("/");
	loginCredentials["company"] = thisURL[thisURL.length-2];

	var login_var_details = "";

	// $scope functions
	$scope.login_func_submitForm = function(){
		loginCredentials["email"] = $scope.login_input_email;
		loginCredentials["password"] = $scope.login_input_pass;

		if(String(loginCredentials["email"]).trim() != "undefined" && String(loginCredentials["password"]).trim() != "undefined"){
			var postData = "skedluxeData="+JSON.stringify(loginCredentials);

			var obj = login_var_details;

			// console.log(obj);

			if(obj.constructor == Array){
				// search for branch with this URL
				for (var i = 0; i < obj.length; i++) {
					if(obj[i]["unique-url"] == loginCredentials["company"]){
						var tmp = obj[i]["company-data"]["employee-accounts"];
						for (var i = 0; i < tmp.length; i++) {
							if(tmp[i]["email-as-username"] == loginCredentials["email"] && tmp[i]["password"] == loginCredentials["password"]){
								// console.log(tmp[i]["email-as-username"] + " " + tmp[i]["password"]);
								break;
							}
							else if(i == (tmp.length-1)){
								$.snackbar({content: "I don't seem to know you.", timeout: 5000});
							}
						}
						break;
					}
				}
			}
			else if(obj.constructor == Object){
				var tmp = obj["company-data"]["employee-accounts"];
				for (var i = 0; i < tmp.length; i++) {
					if(tmp[i]["email-as-username"] == loginCredentials["email"] && tmp[i]["password"] == loginCredentials["password"]){
						// console.log(tmp[i]["email-as-username"] + " " + tmp[i]["password"]);
						break;
					}
					else if(i == (tmp.length-1)){
						$.snackbar({content: "I don't seem to know you.", timeout: 5000});
					}
				}
			}
			else if(obj.constructor == String){
				$location.path("/404");
				$localStorage.$reset();
			}
		}
		else{ $.snackbar({content: "You didn't enter a correct username & password format.", timeout: 5000});}
	}
	function login_func_checkUrl(thisURL){
		var postData = "skedluxeData="+JSON.stringify(loginCredentials);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/url",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){ 
				// console.log(response.data);

				if(String(response.data).trim() != "500"){
					var obj = response.data;
					login_var_details = obj;
					// console.log(obj);

					if(obj.constructor == Array){
						// search for branch with this URL
						for (var i = 0; i < obj.length; i++) {
							if(obj[i]["unique-url"] == thisURL){
								$scope.show_company_welcome = obj[i]["business-name"];

								// console.log($scope.show_company_welcome);
								break;
							}
						}
					}
					else if(obj.constructor == Object){
						$scope.show_company_welcome = obj["business-name"];
						// console.log($scope.show_company_welcome);
					}
					else if(obj.constructor == String){
						$location.path("/404");
						$localStorage.$reset();
					}
				}
				else{ 
					$location.path("/404");
					$localStorage.$reset();
				}
			},
			function errorCallback(response){ $location.path("/"+String(response.status).trim());}
		);
	}

	// life cycle
	$.material.options.autofill = true;
	$.material.init();
	login_func_checkUrl(loginCredentials["company"]);
});

// free user - profile controller
skedluxeApp.controller("skedluxeApp_free_dashboard_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$scope.free_dashboard_var_pending = $localStorage.user_detail_json;

	// $scope functions
	$scope.free_dashboard_func_updatePermit = function(){
		if(String($scope.free_dashboard_var_updatePermit).trim() != "" && $scope.free_dashboard_var_updatePermit != undefined){
			var lists = {
				"userid": $localStorage.user_id, 
				"code": $scope.free_dashboard_var_updatePermit,
				"email": $localStorage.user_email
			};

			var postData = "skedluxeData="+JSON.stringify(lists);
			$http({
				method : "POST",
				url : "http://localhost/narra/skedluxe/api/update-permit-code",
				data: postData,
				headers : {"Content-Type": "application/x-www-form-urlencoded"}
			})
			.then(
				function successCallback(response){
					if(String(response.data).trim() != "200"){
						$.snackbar({content: "Oops, this ticket's code has been sold just now. Please generate a new code.", timeout: 5000});
					}
					else{ 
						$.snackbar({content: "You have successfully submitted a validation request. We will send you updates via email.", timeout: 10000});
						$.snackbar({content: "You will be temporarily logged out of the system to process your request.", timeout: 10000});

						$rootScope.logout_func_trigger();
					}
				},
				function errorCallback(response){
					$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
					$location.path("/404");
				}
			);
		}
		else{ $.snackbar({content: "Sorry, please provide a valid permit code.", timeout: 5000});}
	};

	// life cycle
	$.material.init();
});

// student user - profile controller
skedluxeApp.controller("skedluxeApp_student_dashboard_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$scope.student_dashboard_var_paid = student_dashboard_func_checkPaid($localStorage.user_detail_json);
	// console.log($scope.student_dashboard_var_paid);
	$localStorage.service_id = 3;


	// $scope functions
	function student_dashboard_func_checkPaid(jsontxt){
		try{
			var temp = JSON.parse("["+String(jsontxt).trim()+"]");
			if(temp[0] != null && temp.length >= 1){
				if(temp[0]["student-payment-status"] == true && temp[0]["date-payment-collected"] != ""){ return true;}
				else{ return false;}
			}
			else{ return false;}
		}
		catch(err){ return false;}
	}


	// life cycle
	$.material.options.autofill = true;
	$.material.init();
});

// solo user - profile controller
skedluxeApp.controller("skedluxeApp_soloInstructor_dashboard_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$localStorage.service_id = 4;
	$scope.service_id = $localStorage.service_id;
	$scope.skedluxeApp_enterprise_dashboard_var_activeInclude = "Back-End/pages/includes-src/overview-include.html?" + Math.random();


	// $scope functions
	$scope.skedluxeApp_enterprise_func_redirect = function(page){ $scope.skedluxeApp_enterprise_dashboard_var_activeInclude = page + "?" + Math.random();}


	// life cycle
	$.material.options.autofill = true;
	$.material.init();
});

// business user - profile controller
skedluxeApp.controller("skedluxeApp_business_dashboard_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$localStorage.service_id = 5;
	$scope.service_id = $localStorage.service_id;
	$scope.skedluxeApp_enterprise_dashboard_var_activeInclude = "Back-End/pages/includes-src/overview-include.html?" + Math.random();


	// $scope functions
	$scope.skedluxeApp_enterprise_func_redirect = function(page){ $scope.skedluxeApp_enterprise_dashboard_var_activeInclude = page + "?" + Math.random();}


	// life cycle
	$.material.options.autofill = true;
	$.material.init();
});

// enterprise user - profile controller
skedluxeApp.controller("skedluxeApp_enterprise_dashboard_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$localStorage.service_id = 6;
	$scope.service_id = $localStorage.service_id;
	$scope.skedluxeApp_enterprise_dashboard_var_activeInclude = "Back-End/pages/includes-src/overview-include.html?" + Math.random();


	// $scope functions
	$scope.skedluxeApp_enterprise_func_redirect = function(page){ $scope.skedluxeApp_enterprise_dashboard_var_activeInclude = page + "?" + Math.random();}
		

	// life cycle
	$.material.options.autofill = true;
	$.material.init();
});

// error controller
skedluxeApp.controller("skedluxeApp_error_ctrl", function($scope, $http, $location, $localStorage){
	// life cycle
	$.material.init();
});

// package controller
skedluxeApp.controller("skedluxeApp_generic_package_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$scope.skedluxeApp_generic_package_var_currentForm = "Back-End/pages/includes-src/package-update-package-include.html";

	// $scope functions
	$scope.skedluxeApp_generic_package_editpackage = function(){
		// console.log($scope.nth);
		var lists = {"user_id": $localStorage.user_id, "json_data": JSON.stringify($scope.nth["json_data"]), "archive_id": $scope.nth["archive_id"]};
		var postData = "skedluxeData="+JSON.stringify(lists);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/update-package",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				if(String(response.data).trim() == "200"){ 
					$.snackbar({content: "Package updated successfully.", timeout: 5000});
					// $scope.skedluxeApp_generic_package = {};
					skedluxeApp_generic_package_getJsonFormat();

					skedluxeApp_generic_package_getPackages();
				}
				else{ $.snackbar({content: "Sorry, updating this package failed.", timeout: 5000});}
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}
	$scope.skedluxeApp_generic_package_newPack = function(){
		var lists = {"user_id": $localStorage.user_id, "json_data": JSON.stringify($scope.skedluxeApp_generic_package)};
		if($scope.skedluxeApp_generic_package['package-price-currency'] != "Currency" && $scope.skedluxeApp_generic_package['package-price-currency'] != undefined && $scope.skedluxeApp_generic_package['driving-or-classroom'] != "Type" && $scope.skedluxeApp_generic_package['driving-or-classroom'] != undefined){
			var postData = "skedluxeData="+JSON.stringify(lists);

			$http({
				method : "POST",
				url : "http://localhost/narra/skedluxe/api/new-package",
				data: postData,
				headers : {"Content-Type": "application/x-www-form-urlencoded"}
			})
			.then(
				function successCallback(response){
					// console.log(response.data);
					if(String(response.data).trim() == "200"){ 
						$.snackbar({content: "Package created successfully.", timeout: 5000});
						// $scope.skedluxeApp_generic_package = {};
						skedluxeApp_generic_package_getJsonFormat();

						skedluxeApp_generic_package_getPackages();
					}
					else{ $.snackbar({content: "Sorry, creating this package failed.", timeout: 5000});}
				},
				function errorCallback(response){
					$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
					$location.path("/404");
				}
			);
		}
		else{ $.snackbar({content: "Please select a proper currency and lecture type.", timeout: 5000});}
	}
	$scope.skedluxeApp_generic_package_redirectForm = function(page){ $scope.skedluxeApp_generic_package_var_currentForm = page + "?" + Math.random();}
	$scope.config_dashboard_func_loadPackage = function(nth){ 
		$scope.nth = nth;
		$scope.skedluxeApp_generic_package_var_currentForm = "Back-End/pages/includes-src/package-update-package-include.html";
	}

	$scope.skedluxeApp_generic_package_func_addClass = function(){
		$scope.skedluxeApp_generic_package_var_newClass = {"start-date":"", "end-date":"", "cr-notes":""};
		$scope.skedluxeApp_generic_package_var_currentForm = "Back-End/pages/includes-src/package-add-class-include.html";
	}
	$scope.skedluxeApp_generic_package_func_addClass_submit = function(){
		$scope.nth['json_data']['class-selection'].push($scope.skedluxeApp_generic_package_var_newClass);
		// console.log($scope.nth);
		$scope.skedluxeApp_generic_package_redirectForm("Back-End/pages/includes-src/package-update-package-include.html");
	}

	$scope.skedluxeApp_generic_package_func_addSession = function(){
		$scope.skedluxeApp_generic_package_var_newSession = {"session-date": "",	"start-time": "",	"end-time": "",	"location": "",	"map-link": ""};
		$scope.skedluxeApp_generic_package_var_currentForm = "Back-End/pages/includes-src/package-add-session-include.html?" + Math.random();
	}
	$scope.skedluxeApp_generic_package_func_addSession_submit = function(){
		if(!$scope.ith.hasOwnProperty("session-details")){ $scope.ith["session-details"] = [];}
		$scope.ith["session-details"].push($scope.skedluxeApp_generic_package_var_newSession);

		$scope.skedluxeApp_generic_package_redirectForm("Back-End/pages/includes-src/package-class-session-include.html");
	}

	$scope.skedluxeApp_generic_package_func_loadClass_selection = function(ith, index){
		$scope.ith = ith;
		$scope.index = index;
		// console.log($scope.nth);
		$scope.skedluxeApp_generic_package_redirectForm("Back-End/pages/includes-src/package-edit-class-include.html");
	}
	$scope.skedluxeApp_generic_package_func_loadClass_selection_delete = function(){
		$scope.nth['json_data']['class-selection'].splice([$scope.index],1)
		console.log($scope.nth['json_data']['class-selection']);
	}

	$scope.skedluxeApp_generic_package_func_loadClass_session = function(jth, j_index){
		$scope.jth = jth;
		$scope.j_index = j_index;
		// console.log($scope.nth);
		$scope.skedluxeApp_generic_package_redirectForm("Back-End/pages/includes-src/package-edit-session-include.html");
	}
	$scope.skedluxeApp_generic_package_func_loadClass_session_delete = function(){
		$scope.ith["session-details"].splice([$scope.j_index],1)
		// console.log($scope.nth['json_data']['class-selection']);
	}

	function skedluxeApp_generic_package_getJsonFormat(){
		var lists = {"user_id": $localStorage.user_id};

		var postData = "skedluxeData="+JSON.stringify(lists);

		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/get-packages-format",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				if(String(response.data).trim() != "500"){
					$scope.skedluxeApp_generic_package = response.data[0];
					// console.log(response.data);
				}
				else{ 
					$.snackbar({content: "Unable to fetch you package format.", timeout: 5000}); 
					$location.path("/500");
				}
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}
	function skedluxeApp_generic_package_getPackages(){
		var lists = {"user_id": $localStorage.user_id, "service_id": 12};

		var postData = "skedluxeData="+JSON.stringify(lists);

		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/get-packages",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				if(String(response.data).trim() != "500"){
					$scope.skedluxeApp_generic_packageList = response.data;
				}
				else{ $.snackbar({content: "Unable to fetch you packages. Try creating a new one.", timeout: 5000});}
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}

	// life cycle
	$scope.skedluxeApp_generic_loadbasics();
	skedluxeApp_generic_package_getPackages();
	skedluxeApp_generic_package_getJsonFormat();
});

// company configuration control
skedluxeApp.controller("skedluxeApp_generic_config_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$scope.config_dashboard_var_raw = $localStorage.config_dashboard_var_json;
	if($localStorage.config_dashboard_var_currentCompany == "undefined" || $localStorage.config_dashboard_var_currentCompany === undefined || $localStorage.config_dashboard_var_currentCompany == ""){ $localStorage.config_dashboard_var_currentCompany = 0;}
	else{
		// add "selected attribute to element"
		$("#companyPick").find("option[value=" + String($localStorage.config_dashboard_var_currentCompany).trim() +"]").attr('selected', true);
	}

	// $scope functions
	$scope.config_dashboard_func_loadEmployee = function(nth){ 
		$scope.nth = nth;
		$scope.skedluxeApp_generic_loadbasics();

		$scope.config_dashboard_func_loadEmployee_redirect("Back-End/pages/includes-src/employee-form-personal-details-include.html");
	}
	$scope.config_dashboard_func_loadEmployee_redirect = function(page){ 
		// generate random get request so the page wont be cached
		$scope.skedluxeApp_enterprise_dashboard_var_employeeForm = page + "?" + Math.random();
	}
	$scope.config_dashboard_func_changeCompany = function(){
		$localStorage.config_dashboard_var_currentCompany = $scope.config_dashboard_var_companyIndex;
		config_dashboard_func_getValidJson($localStorage.user_id);
	}
	$scope.skedluxeApp_generic_loadbasics = function(){
		$.material.options.autofill = true;
		$.material.init();
		$("select").dropdown();
		
		setTimeout(function() {$("textarea").trigger("change");}, 200);
	}
	$scope.config_dashboard_func_getUpdated = function(){

		if($localStorage.service_id == 6){
			$localStorage.config_dashboard_var_json[parseInt($localStorage.config_dashboard_var_currentCompany)] = $scope.config_dashboard_var_raw;
			$scope.config_dashboard_var_raw = $localStorage.config_dashboard_var_json;
		}

		// console.log($scope.config_dashboard_var_raw);

		var lists = {"user_id": $localStorage.user_id, "service_id": $localStorage.service_id, "json_data": JSON.stringify($scope.config_dashboard_var_raw)};
		approved = true;

		if($localStorage.service_id == 6){
			if($scope.config_dashboard_var_raw[0]['unique-url'] != $scope.config_dashboard_var_raw[1]['unique-url'] || 
			(String($scope.config_dashboard_var_raw[0]['unique-url']).trim() == "" && String($scope.config_dashboard_var_raw[1]['unique-url']).trim() == "")
			){ approved = true; }
			else{ approved = false; $.snackbar({content: "Change one of your URLs.", timeout: 5000}); }
		}

		if(approved == true){
			var postData = "skedluxeData="+JSON.stringify(lists);
			$http({
				method : "POST",
				url : "http://localhost/narra/skedluxe/api/update-settings-conf",
				data: postData,
				headers : {"Content-Type": "application/x-www-form-urlencoded"}
			})
			.then(
				function successCallback(response){
					// console.log(response.data);
					if(String(response.data).trim() == "500"){ $.snackbar({content: "Update failed. Someone has already used this URL.", timeout: 5000});}
					else{ 
						config_dashboard_func_getValidJson($localStorage.user_id);
						$.snackbar({content: "Update successful. Thank you.", timeout: 5000});
					}
				},
				function errorCallback(response){
					$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
					$location.path("/404");
				}
			);
		}
		else{ $.snackbar({content: "Sorry, I am Unable to process this request.", timeout: 5000});}
	}
	$scope.config_dashboard_func_createConfig = function(){
		var lists = {"user_id": $localStorage.user_id, "service_id": $localStorage.service_id};

		var postData = "skedluxeData="+JSON.stringify(lists);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/new-settings-conf",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				config_dashboard_func_getValidJson($localStorage.user_id);
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}
	function config_dashboard_func_getValidJson(user_id){
		var lists = {"user_id": user_id, "service_id": $localStorage.service_id};

		var postData = "skedluxeData="+JSON.stringify(lists);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/service-json",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				if(String(response.data).trim() != "" && String(response.data).trim() != "500"){ 
					$localStorage.config_dashboard_var_json = JSON.parse(response.data);
					$scope.config_dashboard_var_showform = true;

					if($localStorage.service_id == 6){
						$scope.config_dashboard_var_raw = $localStorage.config_dashboard_var_json[parseInt($localStorage.config_dashboard_var_currentCompany)];
					}
					else{ $scope.config_dashboard_var_raw = $localStorage.config_dashboard_var_json;}
				}
				else{ $scope.config_dashboard_var_showform = false;}

				// console.log($localStorage.config_dashboard_var_showform);
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}


	// life cycle
	$scope.skedluxeApp_generic_loadbasics();
	config_dashboard_func_getValidJson($localStorage.user_id);
});

skedluxeApp.controller("skedluxeApp_generic_overview_ctrl", function($scope, $http, $location, $localStorage, $rootScope){
	// initialize local variables
	$scope.show_schools = true;
	$scope.show_package = true;
	$scope.show_instructor = true;

	// $scope functions
	$scope.skedluxeApp_generic_loadbasics = function(){
		$.material.options.autofill = true;
		$.material.init();
		$("select").dropdown();
		
		setTimeout(function() {$("textarea").trigger("change");}, 200);
	}
	function skedluxeApp_generic_overview_loadCompleteList(user_id){
		var lists = {"user_id": user_id};

		var postData = "skedluxeData="+JSON.stringify(lists);
		$http({
			method : "POST",
			url : "http://localhost/narra/skedluxe/api/list/get-overview-list",
			data: postData,
			headers : {"Content-Type": "application/x-www-form-urlencoded"}
		})
		.then(
			function successCallback(response){
				// console.log(response.data);
				if(String(response.data).trim() != "" && String(response.data).trim() != "500"){ 
					$scope.school_instructor_packages = response.data;
					console.log($scope.school_instructor_packages);
				}
				else{ $.snackbar({content: "Sorry!, Unable to load the data.", timeout: 5000});}
			},
			function errorCallback(response){
				$.snackbar({content: "Oops, I am detecting an improper manner of entry. I am considering this as harmful.", timeout: 5000});
				$location.path("/404");
			}
		);
	}

	// life cycle
	$scope.skedluxeApp_generic_loadbasics();
	skedluxeApp_generic_overview_loadCompleteList($localStorage.user_id);
});