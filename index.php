
<?php 

	// Routing page for default URLs.

	require "php-plugins/AltoRouter.php";
	require "php-plugins/PHPMailer-master/PHPMailerAutoload.php";

	$router = new AltoRouter();

	// for STAGING
	// $router->setBasePath('');

	// for DEV
	$router->setBasePath('/NARRA/skedluxe');


	// FRONT END - valid URLs -------------------------------------------
		// "Home" valid URLs
		$router->map("GET", "/", "Front-End/index.html");
		$router->map("GET", "/home", "Front-End/index.html");

		// "Pricing" valid URLs
		$router->map("GET", "/pricing", "Front-End/pricing.html");

		// old URL
		$router->map("GET", "/oldFiles", "oldFiles/index.html");


	// BACK END - valid URLs -------------------------------------------
		// "default" - 404
		$router->map("GET", "/skl", "Back-End/index.html");
		$router->map("GET", "/skl/[**:trailing]", function($trailing){ header("Location: /skl"); die();});

		//  trailing pages
		$router->map("GET", "/skl#/signup", "Back-End/index.html");
		$router->map("GET", "/skl#/login", "Back-End/index.html");


	// REST api - valid URLs -------------------------------------------
		
		// submit new users
		$router->map("POST", "/api/signup", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			// check if permit code exists or expired
			$goodPermitCode = false;
			if($skedluxeData["permitcode"] != "undefined" && $skedluxeData["permitcode"] != ""){
				$strCount = mysqli_query($openDB, "SELECT * FROM `skedluxe_permit_codes` WHERE `permit_code`='".$skedluxeData["permitcode"]."' and `permit_code` not in (select `permit_code` from `skedluxe_users`)");
				if(mysqli_num_rows($strCount) == 1){ $goodPermitCode = true;}
			}
			else{ $goodPermitCode = true;}
			
			
			if($goodPermitCode){
				$str = "INSERT INTO `skedluxe_users`(`username`, `password`, `fullname`, `user_contact_no`, `user_email`, `user_license_no`, `permit_code`) VALUES (
				'".mysqli_escape_string($openDB, $skedluxeData["username"])."',
				'".mysqli_escape_string($openDB, $skedluxeData["password"])."',
				'".mysqli_escape_string($openDB, $skedluxeData["fullname"])."',
				'".mysqli_escape_string($openDB, $skedluxeData["contact"])."',
				'".mysqli_escape_string($openDB, $skedluxeData["email"])."',
				".(mysqli_escape_string($openDB, $skedluxeData["license"]) == "undefined" || mysqli_escape_string($openDB, $skedluxeData["license"]) == "" ? "null": "'".mysqli_escape_string($openDB, $skedluxeData["license"])."'").",
				".(mysqli_escape_string($openDB, $skedluxeData["permitcode"]) == "undefined" || mysqli_escape_string($openDB, $skedluxeData["permitcode"]) == "" ? "'free'": "'".mysqli_escape_string($openDB, $skedluxeData["permitcode"])."'").")";
				$executeQuery = mysqli_query($openDB, $str);

				// insert new default data template to new user

				if($executeQuery){ 
					echo "200";

					$header = "Welcome!";
					$body = "You have been successfully registered at <a href=\"http://skedluxe.com\">Skedluxe.com</a>.<br>Login to your <a href=\"http://skedluxe.com/skl#/login\">account</a> now!";
					$footer = "Thank you for partnering with us. We hope to give you the best service we can, so feel free to send your suggestions to <a href=\"#\"><b>Skedluxe</b> Hotline</a>. We will get back at you soonest.";
					$subj = "Signup Confirmation";

					$template = emailConfirmation($header, $body, $footer);
					sendMail($skedluxeData["email"], $template, $subj);
				}
				else{ 
					// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
					echo "500";
				}
			}
			else{ echo "500";}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// submit new permit code
		$router->map("POST", "/api/new-permit-code", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);
			
			$str = "INSERT INTO `skedluxe_permit_codes`(`permit_code`, `plan_id`, `date_availed`, `date_expire`) VALUES ('".mysqli_escape_string($openDB, $skedluxeData["code"])."',".mysqli_escape_string($openDB, $skedluxeData["id"]).", current_date, DATE_ADD(current_date, INTERVAL 45 DAY))";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){ echo "200";}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// submit new setting configuration
		$router->map("POST", "/api/new-settings-conf", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "INSERT INTO `skedluxe_user_services`(`service_id`, `user_id`, `archive_json_data`) VALUES (".mysqli_escape_string($openDB, $skedluxeData["service_id"]).", ".mysqli_escape_string($openDB, $skedluxeData["user_id"]).", (SELECT `service_valid_json_form` FROM `skedluxe_services` WHERE `service_id`=".mysqli_escape_string($openDB, $skedluxeData["service_id"])."))";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){ echo "200";}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// submit new package
		$router->map("POST", "/api/new-package", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "INSERT INTO `skedluxe_user_services`(`service_id`, `user_id`, `archive_json_data`) VALUES (12, ".mysqli_escape_string($openDB, $skedluxeData["user_id"]).", '".mysqli_escape_string($openDB, $skedluxeData["json_data"])."')";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){ echo "200";}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// update permit code
		$router->map("POST", "/api/update-permit-code", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);
			
			// check if permit code exists or used
			$goodPermitCode = false;
			if($skedluxeData["code"] != "undefined" && $skedluxeData["code"] != "" && $skedluxeData["code"] != "free"){
				$strCount = mysqli_query($openDB, "SELECT * FROM `skedluxe_permit_codes` WHERE `permit_code`='".mysqli_escape_string($openDB, $skedluxeData["code"])."' and `permit_code` not in (SELECT `permit_code` FROM `skedluxe_users`)");
				if(mysqli_num_rows($strCount) == 1){ $goodPermitCode = true;}
			}

			if($goodPermitCode){
				$str = "UPDATE `skedluxe_users` SET `permit_code`='".mysqli_escape_string($openDB, $skedluxeData["code"])."', `user_detail_json`='free to [".mysqli_escape_string($openDB, $skedluxeData["code"])."]' WHERE BINARY `user_id`=".mysqli_escape_string($openDB, $skedluxeData["userid"]);
				$executeQuery = mysqli_query($openDB, $str);

				// insert defeult data templates to new plan

				if($executeQuery){ 
					echo "200";

					$header = "Howdy!";
					$body = "You have been successfully upgraded your permit code to ".$skedluxeData["code"].". Log into your <a href=\"http://skedluxe.com/skl#/login\">account</a> now and see your updated features.";
					$footer = "Thank you for partnering with us. We hope to give you the best service we can, so feel free to send your suggestions to <a href=\"#\"><b>Skedluxe</b> Hotline</a>. We will get back at you soonest.";
					$subj = "Signup Confirmation";

					$template = emailConfirmation($header, $body, $footer);
					sendMail($skedluxeData["email"], $template, $subj);
				}
				else{ 
					// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
					echo "300";
				}
			}
			else{ echo "500";}
			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// update settings configuration
		$router->map("POST", "/api/update-settings-conf", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			// check if url is unique
			$getUnix = "";
			$uniqueURL = "";
			if($skedluxeData["service_id"] == 6){
				$getUnix = json_decode($skedluxeData["json_data"], true);
				$whereClause = "";
				for ($i=0; $i < count($getUnix); $i++) { 
					if (next($getUnix) === false) {
						$whereClause = $whereClause."`archive_json_data` like '%\"unique-url\":\"".mysqli_escape_string($openDB, $getUnix[$i]["unique-url"])."\",%'";
					}
					else{
						$whereClause = $whereClause."`archive_json_data` like '%\"unique-url\":\"".mysqli_escape_string($openDB, $getUnix[$i]["unique-url"])."\",%' or ";
					}
				}
				$unix = "SELECT * FROM `skedluxe_user_services` WHERE (".$whereClause.") and `user_id` <> ".mysqli_escape_string($openDB, $skedluxeData["user_id"])." and `archive_json_data` not like '%\"unique-url\":\"\",%'";
				$uniqueURL = mysqli_query($openDB, $unix);
			}
			else if($skedluxeData["service_id"] == 4 || $skedluxeData["service_id"] == 5){
				$getUnix = json_decode($skedluxeData["json_data"], true)["unique-url"];
				$unix = "SELECT * FROM `skedluxe_user_services` WHERE `archive_json_data` like '%\"unique-url\":\"".mysqli_escape_string($openDB, $getUnix)."\",%' and `user_id` <> ".mysqli_escape_string($openDB, $skedluxeData["user_id"])." and `archive_json_data` not like '%\"unique-url\":\"\",%'";
				$uniqueURL = mysqli_query($openDB, $unix);
			}
			
			// echo $unix;
			if($skedluxeData["service_id"] == 3){ $approved = true;}
			else{ 
				if(mysqli_num_rows($uniqueURL) == 0){ $approved = true;}
				else{ $approved = false;}
			}
			if($approved){
				$str = "UPDATE `skedluxe_user_services` SET `archive_json_data`='".mysqli_escape_string($openDB, $skedluxeData["json_data"])."' WHERE `user_id`=".mysqli_escape_string($openDB, $skedluxeData["user_id"])." and `service_id`=".mysqli_escape_string($openDB, $skedluxeData["service_id"]);
				$executeQuery = mysqli_query($openDB, $str);

				if($executeQuery){ echo "200";}
				else{ 
					// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
					echo "500";
				}
			}
			else{ echo "500";}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// update package
		$router->map("POST", "/api/update-package", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "UPDATE `skedluxe_user_services` SET `archive_json_data`='".mysqli_escape_string($openDB, $skedluxeData["json_data"])."' WHERE `user_id`=".mysqli_escape_string($openDB, $skedluxeData["user_id"])." and `service_id`=12 and `archive_id`=".mysqli_escape_string($openDB, $skedluxeData["archive_id"]);
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){ echo "200";}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}
			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get single user
		$router->map("POST", "/api/list/user", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);
			
			$str = "SELECT `user_id`, `username`, `password`, `fullname`, `user_contact_no`, `user_email`, `user_license_no`, `permit_code`, `user_detail_json`, `plan_name`, group_concat(`access_url` separator '/') as `access_url` FROM `view_userinfo` WHERE BINARY `username`='".mysqli_escape_string($openDB, $skedluxeData["username"])."' and `password`='".mysqli_escape_string($openDB, $skedluxeData["password"])."' limit 1";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();

				// arr[0]: count user result
				array_push($arr, mysqli_num_rows($executeQuery));

				while ($row = mysqli_fetch_array($executeQuery)) { 
					// arr[1]: current user id
					array_push($arr, $row["user_id"]);
					array_push($arr, $row["username"]);
					array_push($arr, $row["password"]);
					array_push($arr, $row["fullname"]);
					array_push($arr, $row["user_contact_no"]);
					array_push($arr, $row["user_email"]);
					array_push($arr, $row["user_license_no"]);
					array_push($arr, $row["user_detail_json"]);
					array_push($arr, $row["permit_code"]);
					array_push($arr, $row["plan_name"]);
					array_push($arr, $row["access_url"]);
				}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get single url
		$router->map("POST", "/api/list/url", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);
			
			$str = "SELECT `archive_json_data` FROM `skedluxe_user_services` WHERE BINARY `archive_json_data` like '%\"unique-url\":\"".mysqli_escape_string($openDB, $skedluxeData["company"])."\",%'";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				if(mysqli_num_rows($executeQuery) == 1){
					while ($row = mysqli_fetch_array($executeQuery)) { 
						echo $row["archive_json_data"];
					}
				}
				else{ echo "500";}
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get service valid json
		$router->map("POST", "/api/list/service-json", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "SELECT `archive_json_data` FROM `skedluxe_user_services` WHERE `user_id`=".mysqli_escape_string($openDB, $skedluxeData["user_id"])." and `service_id`=".mysqli_escape_string($openDB, $skedluxeData["service_id"])." LIMIT 1";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) { array_push($arr, $row["archive_json_data"]);}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get roles
		$router->map("POST", "/api/list/roles", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$str = "SELECT `plan_name` FROM `skedluxe_plans` order by `plan_id` asc";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) { array_push($arr, $row["plan_name"]);}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get plans
		$router->map("POST", "/api/list/plans", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$str = "SELECT `plan_id`, `plan_name`, `plan_details_json`, `billyearly_monthly_cost`, `billmonthly_monthly_cost` FROM `skedluxe_plans` ORDER BY `billmonthly_monthly_cost`";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) { 
					$arrData = array();
					$arrData["id"] = $row["plan_id"];
					$arrData["name"] = $row["plan_name"];
					$arrData["inclusions"] = $row["plan_details_json"];
					$arrData["billed_monthly"]= $row["billmonthly_monthly_cost"];
					$arrData["billed_annually"] = $row["billyearly_monthly_cost"];

					array_push($arr, $arrData);
				}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get & send password to email
		$router->map("POST", "/api/list/recover/credential", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);
			
			$str = "SELECT `username`, `password` FROM `skedluxe_users` WHERE BINARY `user_email` = '".mysqli_escape_string($openDB, $skedluxeData["email"])."' limit 1";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery and mysqli_num_rows($executeQuery) == 1){ 

				$arrData = array();
				while ($row = mysqli_fetch_array($executeQuery)) { 
					$arrData["username"] = $row["username"];
					$arrData["password"] = $row["password"];
				}
				echo "200";

				$header = "Howdy!";
				$body = "We have successfully verified your account registration at <a href=\"http://skedluxe.com\">Skedluxe.com</a>. Here is your login username and password.<br><br>Username: ".$arrData["username"]."<br>Password: ".$arrData["password"];
				$footer = "Thank you for partnering with us. We hope to give you the best service we can, so feel free to send your suggestions to <a href=\"#\"><b>Skedluxe</b> Hotline</a>. We will get back at you soonest.";
				$subj = "User Recovery";

				$template = emailConfirmation($header, $body, $footer);
				sendMail($skedluxeData["email"], $template, $subj);
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get all packages per user
		$router->map("POST", "/api/list/get-packages", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "select * from `skedluxe_user_services` where `service_id`=12 and `user_id`=".mysqli_escape_string($openDB, $skedluxeData["user_id"]);
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) { 
					$arrData = array();
					$arrData["archive_id"] = $row["archive_id"];
					$arrData["json_data"] = json_decode($row["archive_json_data"], true);

					array_push($arr, $arrData);
				}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get packages json format
		$router->map("POST", "/api/list/get-packages-format", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "select `service_valid_json_form` from `skedluxe_services` where `service_id`=12 limit 1";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) { 
					array_push($arr, json_decode($row["service_valid_json_form"], true));
				}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});

		// get all all school/instructor/package data
		$router->map("POST", "/api/list/get-overview-list", function(){
			$openDB = openDB("localhost", "root", "", "skedluxe_groupware");
			// $openDB = openDB("50.116.84.58", "roadtest_adean", "newnieMendoza", "roadtest_skedluxe");
			// -----------------------------------------------------------------

			$skedluxeData = json_decode($_POST["skedluxeData"], true);

			$str = "SELECT * FROM `skedluxe_user_services` WHERE `service_id` in (4,5,6,12)";
			$executeQuery = mysqli_query($openDB, $str);

			if($executeQuery){
				$arr = array();
				while ($row = mysqli_fetch_array($executeQuery)) {
					$temp = "{\"user_id\":".$row["user_id"].", \"service_id\":".$row["service_id"].", \"json_data\":".$row["archive_json_data"]."}";
					array_push($arr, json_decode($temp, true));
				}
				print_r(json_encode($arr));
			}
			else{ 
				// either SYNTAX ERROR, FOREIGN KEY CONSTRAINT or SQL query not parsed correctly.
				echo "500";
			}

			// -----------------------------------------------------------------
			closeDB($openDB);
		});


	// -----------------------------------------------------------------



	$match = $router->match();
	
	if($match && is_callable($match['target'])){ call_user_func_array($match['target'], $match['params']);}
	else if($match && is_file($match['target'])){
		$_GET= $match['params'];
		require $match["target"];
	}
	else{ require "Back-End/index.html";}


	// ------------------------------------------------------------------------------------------------------
	// open connection to database
	function openDB($ip, $user, $pass, $dbname){
		$openDB = mysqli_connect($ip, $user, $pass, $dbname);

		if (mysqli_connect_errno()) {
			// $actual_openDB = "http://".$_SERVER[HTTP_HOST]."".$_SERVER[REQUEST_URI];
			$actual_openDB = "http://".$_SERVER[HTTP_HOST];
			// header("Location: ".$actual_openDB."/DATABASE_ERROR.html");
		    return null;
		}

		return $openDB;
	}

	// close existing connection to database
	function closeDB($openDB){ mysqli_close($openDB);}

	// send emails
	function sendMail($recipient, $message, $subject){
		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com;smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "netzon.adean@gmail.com";
		$mail->Password = "samatosapassword";
		$mail->SMTPSecure = "ssl";
		$mail->Port = 465;

		$mail->setFrom("netzon.adean@gmail.com");
		$mail->addAddress($recipient);
		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body    = $message;
		$mail->AltBody = "";

		if(!$mail->send()){ 
			// echo $mail->ErrorInfo;
		};
	}

	// email HTML template
	function emailConfirmation($content_Header, $content_Body, $content_Footer){
		$template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
						<html lang=\"en\">
							<head>
								<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
								<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> 
								<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> 
								<meta name=\"format-detection\" content=\"telephone=no\"> 
								<title>Skedluxe Directory</title>
								<style type=\"text/css\">
									body {
										margin: 0;
										padding: 0;
										-ms-text-size-adjust: 100%;
										-webkit-text-size-adjust: 100%;
									}
									table { border-spacing: 0; }
									table td { border-collapse: collapse; }
									.ExternalClass { width: 100%; }
									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div { line-height: 100%; }
									.ReadMsgBody {
										width: 100%;
										background-color: #ebebeb;
									}
									table {
									mso-table-lspace: 0pt;
									mso-table-rspace: 0pt;
									}
									img { -ms-interpolation-mode: bicubic; }
									.yshortcuts a { border-bottom: none !important; }
									@media screen and (max-width: 599px) {
										.force-row,
										.container {
											width: 100% !important;
											max-width: 100% !important;
										}
									}
									@media screen and (max-width: 400px) {
										.container-padding {
											padding-left: 12px !important;
											padding-right: 12px !important;
										}
									}
									.ios-footer a {
										color: #aaaaaa !important;
										text-decoration: underline;
									}
								</style>
							</head>

							<body style=\"margin:0; padding:0;\" bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">

								<!-- 100% background wrapper (grey background) -->
								<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\">
									<tr>
										<td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color: #F0F0F0;\">
											<!-- 600px container (white background) -->
											<table border=\"0\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" class=\"container\" style=\"width:600px;max-width:600px\">
												<tr>
													<td class=\"container-padding header\" align=\"left\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:34px;padding:20px;\">
														<center><b>Skedluxe</b>&nbsp;Directory</center>
													</td>
												</tr>
												<tr>
													<td class=\"container-padding content\" align=\"left\" style=\"padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff\">
														<br>
														<div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550\">".$content_Header."</div>
														<br></br>
														<div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#333333\">
															".$content_Body."
														</div>
														<br>
														<div class=\"hr\" style=\"height:1px;border-bottom:1px solid #cccccc\">&nbsp;</div><br>
														<div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333\">
															<span style=\"font-size:11px;\">".$content_Footer."</span>
														</div>
														<br>
													</td>
												</tr>
												<tr>
													<td class=\"container-padding footer-text\" align=\"left\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding:10px 20px 20px 20px;\">
														<br>
														Skedluxe Groupware © 2016
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

							</body>
						</html>";

		return $template;
	}
 ?>



